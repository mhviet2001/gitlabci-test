# Use Node.js Alpine as the base image
FROM node:alpine

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install the application dependencies
RUN npm install

# Copy the application source code to the working directory
COPY . .

# Expose port 3000 to listen for requests
EXPOSE 3000

# Run the Node.js application when the container is started
CMD ["node", "app.js"]
