// Create a simple HTTP server
import { createServer } from 'http';

const server = createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello, World!\n');
});

// Listen on port 3000 for incoming requests
server.listen(3000, () => {
    console.log('Server is running on port 3000');
});
